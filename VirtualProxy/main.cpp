#include <iostream>
#include "personne.h"
#include "individu.h"
#include "proxyindividu.h"

#include<string>


using namespace std;

int main()
{
    Personne* monsieurA= new ProxyIndividu();
    cout<<"\n";
    monsieurA->Parler();
    cout<<"\n";

    cout<<"Mais c'est urgent!";
    cout<<"\n";
    monsieurA->Discuter("Mais c'est urgent!");
    cout<<"\n";
    cout<<"\n";

    delete monsieurA;

    return 1;
}

