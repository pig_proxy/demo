TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    individu.cpp \
    proxyindividu.cpp \
    personne.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    individu.h \
    proxyindividu.h \
    personne.h

