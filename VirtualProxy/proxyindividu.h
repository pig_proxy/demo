#ifndef PROXYINDIVIDU_H
#define PROXYINDIVIDU_H
#include "individu.h"
#include<iostream>
#include<string>
using namespace std;

class ProxyIndividu : public Personne
{
    Individu* sub;
public:
    ProxyIndividu();
    void Parler() {
        cout<<"je n'ai pas le temps de parler // c'est le proxy qui parle";
    }
    void Discuter(string mots) {
        if (sub==NULL){
            sub= new Individu();
        }
        sub->Discuter(mots);

    }
    ~ProxyIndividu(){
        delete sub;
    }

};

#endif // PROXYINDIVIDU_H
