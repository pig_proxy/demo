#ifndef PERSONNE_H
#define PERSONNE_H

#include<iostream>
#include<string>
using namespace std;

class Personne
{
public:
    Personne();
    virtual void Parler(void)=0;
    virtual void Discuter(string mots)=0;
    virtual ~Personne(){}
};

#endif // PERSONNE_H
