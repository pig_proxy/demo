#include "permissionproxy.h"

permissionProxy::permissionProxy(int n){grade=n;}

permissionProxy::~permissionProxy(){}

void permissionProxy::modifierUserInfo()
{
    cout<<"Proxy vous permet de modifier votre information personelles. Tous nos adhesion peuvent y aller."<<endl;
    permission.modifierUserInfo();
}

void permissionProxy::supprimerNote()
{
    if (grade>=10)
    {
        cout<<"Proxy vous permet de supprimer la note avec un grade "<<grade<<" superieur a 10."<<endl;
        permission.supprimerNote();
    }
    else
        cout<<"Proxy ne vous permet pas de supprimer la note, parce que votre grade "<<grade<<" est inferieur a 10"<<endl<<endl;
}
