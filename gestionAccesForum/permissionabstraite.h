#ifndef PERMISSIONABSTRAITE
#define PERMISSIONABSTRAITE

#include<iostream>
using namespace std;

class permissionAbstraite
{
    public:
        virtual void modifierUserInfo()=0;
        virtual void supprimerNote()=0;
};

#endif // PERMISSIONABSTRAITE
