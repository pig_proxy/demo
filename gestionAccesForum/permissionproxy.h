#ifndef PERMISSIONPROXY_H
#define PERMISSIONPROXY_H

#include "permissionreel.h"

class permissionProxy:public permissionAbstraite
{
    private:
        permissionReel permission;
        int grade;
    public:
        permissionProxy(int);
        ~permissionProxy();
        void modifierUserInfo();
        void supprimerNote();
};

#endif // PERMISSIONPROXY_H
