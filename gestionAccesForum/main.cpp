#include "permissionproxy.h"

int main()
{
    permissionAbstraite *client1=new permissionProxy(9);
    client1->modifierUserInfo();
    client1->supprimerNote();

    permissionAbstraite *client2=new permissionProxy(12);
    client2->modifierUserInfo();
    client2->supprimerNote();

    return 0;
}
