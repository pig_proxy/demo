#ifndef PERMISSIONREEL_H
#define PERMISSIONREEL_H

#include "permissionabstraite.h"

class permissionReel: public permissionAbstraite
{
    public:
        permissionReel();
        ~permissionReel();

        void modifierUserInfo();
        void supprimerNote();
};

#endif // PERMISSIONREEL_H
