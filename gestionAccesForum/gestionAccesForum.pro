TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    permissionreel.cpp \
    permissionproxy.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    permissionabstraite.h \
    permissionreel.h \
    permissionproxy.h

